# Commiting changes

Before commiting changes to our repository, we need to understand what a commit is and what it
is composed of. There are three basic objects in git:

- commit
- tree
- blob

Everyone of these object is identified by a hash. You can inspect them with `git cat-file -p <hash>`.

## The commit

A commit is one of the underlying objects in git. It contains information about the tracked
version of the file. Namely these are:

- The commit hash
- An author
- A commiter
- A date
- A message
- A parent commit
- Something called a tree

## The tree

The tree is something really simple. It contains your directories. When you inspect such a tree,
there is a list of other objects, trees and blobs. They also have metadata like the POSIX file mode,
their name, what kind of object they represent and what hash this object is identified by.

## The blob

The blob is like a file. It contains your information, your data. But only that. Here only the
files content is saved. No metadata or anything.

